
#include <ArduinoJson.h>
#include <dht.h>

dht DHT;

#define DHT11_PIN 7

void setup(){
  Serial.begin(9600);
  Serial.flush();
}
char quote = '"';
void loop()
{
  int chk = DHT.read11(DHT11_PIN);
  double tsen = DHT.temperature + 273;

  double telem = DHT.humidity;

  delay(1000);
  
  StaticJsonBuffer<200> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  root["device_tsen_temp_000"] = tsen;
  root["device_telem_vdouble"] = telem;

  root.printTo(Serial);
  Serial.print("\n");

  delay(500);
  String plain_text = "Temperature: "
                + String(tsen - 273)
                + "ºC Humidity: "
                + String(telem)
                + "%";
  Serial.println(plain_text);
  Serial.print("\n");

  //or

  /*
   * String cosmos_value =  "{\"device_tsen_temp_000\":" 
                + String(tsen) 
                + ",\"device_telem_vdouble\":}"
                + String(telem);
  Serial.println(cosmos_value);
  
  delay(1000);
   */


}

